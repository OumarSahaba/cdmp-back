package sn.modelsis.cdmp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import sn.modelsis.cdmp.entities.DemandeAdhesion;

public interface DemandeAdhesionRepository extends JpaRepository<DemandeAdhesion,Long> {
}
